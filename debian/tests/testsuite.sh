#!/bin/bash

set -eu

# info_hash is URL-encoded hash 123456789abcdef123456789abcdef123456789a
info_hash="%124Vx%9A%BC%DE%F1%23Eg%89%AB%CD%EF%124Vx%9A"

for _ in $(seq 4); do
	ip="$(( $RANDOM & 0xf )).$(( $RANDOM & 0xf )).13.16"
	port="$(( $RANDOM & 0xff ))"
	request_url="/announce?info_hash=$info_hash&ip=$ip&port=$port"

	for host in "127.0.0.1:6969" "[::1]:6969"
	do
		request="$host$request_url"
		echo "$request"
		curl -s "$request" -D - -S -o /dev/null
	done
done
