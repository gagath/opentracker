From: Agathe Porte <gagath@debian.org>
Date: Sat, 25 May 2024 16:36:07 +0200
Subject: man: drop empty Nm sections

Nm seems to be used for automatic line numbering and should not be used
alone.
---
 man1/opentracker.1      | 9 ---------
 man4/opentracker.conf.4 | 2 --
 2 files changed, 11 deletions(-)

diff --git a/man1/opentracker.1 b/man1/opentracker.1
index 0103ebe..319ee84 100644
--- a/man1/opentracker.1
+++ b/man1/opentracker.1
@@ -5,7 +5,6 @@
 .Nm opentracker
 .Nd a free and open bittorrent tracker
 .Sh SYNOPSIS
-.Nm
 .Op Fl f Ar config
 .Op Fl i Ar ip-select
 .Op Fl p Ar port-bind-tcp
@@ -16,7 +15,6 @@
 .Op Fl u Ar user
 .Op Fl w| Fl b accesslist
 .Sh DESCRIPTION
-.Nm
 is a bittorrent tracker that implements announce and scrape actions over the
 UDP and the plain http protocol, aiming for minimal resource usage.
 .Pp
@@ -62,11 +60,9 @@ Set an ip address in IPv4 or IPv6 or a net in CIDR notation to bless the network
 for access to restricted resources.
 .It Fl r Ar redirect-url
 Set the URL that
-.Nm
 will redirect users to when the / address is requested via HTTP.
 .It Fl d Ar chdir
 Sets the directory
-.Nm
 will
 .Xr chroot 2
 to if ran as root or
@@ -75,11 +71,9 @@ to if ran as unprivileged user. Note that any accesslist files need to be
 relative to and within that directory.
 .It Fl u Ar user
 User to run
-.Nm
 under after all operations that need privileges have finished.
 .It Fl w Ar accesslist | Fl b Ar accesslist
 If
-.Nm
 has been compiled with the
 .B WANT_ACCESSLIST_BLACK
 or
@@ -88,12 +82,10 @@ options, this option sets the location of the accesslist.
 .El
 .Sh EXAMPLES
 Start
-.Nm
 bound on UDP and TCP ports 6969 on IPv6 localhost.
 .Dl # ./opentracker -i ::1 -p 6969 -P 6969
 .Pp
 Start
-.Nm
 bound on UDP port 6868 and TCP port 6868 on IPv4 localhost and allow
 privileged access from the network 192.168/16 while redirecting
 HTTP clients accessing the root directory, which is not covered by the
@@ -105,7 +97,6 @@ udp://192.168.0.4:6868/announce respectively.
 .Bl -tag -width indent
 .It Pa opentracker.conf
 The
-.Nm
 config file.
 .El
 .Sh SEE ALSO
diff --git a/man4/opentracker.conf.4 b/man4/opentracker.conf.4
index 2bc1389..9b61ad1 100644
--- a/man4/opentracker.conf.4
+++ b/man4/opentracker.conf.4
@@ -5,10 +5,8 @@
 .Nm opentracker.conf
 .Nd configuration file for opentracker
 .Sh SYNOPSIS
-.Nm
 .Sh DESCRIPTION
 The
-.Nm
 configuration file specifies various options for configuring the behavior of the opentracker program.
 .Pp
 Lines starting with '#' are comments and are ignored. Options are specified as 'keyword value' pairs.
